import { DataState } from "../enum/data-state.enum";

export interface AppState<T> {

    dataState: DataState;
    appData?: T;   //am pus ? dupa numele parametrului deoarece ? inseamna ca este optional
    error?: string;
}