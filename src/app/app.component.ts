import { Component, ErrorHandler, OnInit } from '@angular/core';
import { BehaviorSubject, catchError, map, Observable, of, startWith } from 'rxjs';
import { DataState } from './enum/data-state.enum';
import { Status } from './enum/status.enum';
import { AppState } from './interface/app-state';
import { CustomResponse } from './interface/custom-response';
import { ServerService } from './service/server.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


export class AppComponent implements OnInit {

  appState$: Observable<AppState<CustomResponse>>;
  readonly DataState = DataState;
  readonly Status = Status;
  private filterSubject = new BehaviorSubject<string>('');
  private dataSubject = new BehaviorSubject<CustomResponse>(null);  
  filterStatus$ = this.filterSubject.asObservable();


  constructor(private serverService: ServerService) { }


  //once this component it's initialized it's going to call this method
  //to run the host on angular just run this command: ng serve - now a process is running on port 4200 - angular 
  ngOnInit(): void {
    this.appState$ = this.serverService.servers$
      .pipe(
        map(response => {
          this.dataSubject.next(response)
          return {dataState: DataState.LOADED_STATE, appData: response }
        }),
        startWith({ dataState: DataState.LOADING_STATE }),
        catchError((errorRespone: string) => {
          return of({ dataState: DataState.ERROR_STATE, error: errorRespone })
        })
      );
  }

  pingServer(ipAddress:string): void{
        this.filterSubject.next(ipAddress);
        this.appState$ = this.serverService.ping$(ipAddress)
          .pipe(
            map(response => {
              const index =  this.dataSubject.value.data.servers.findIndex(
                  elem => elem.id === response.data.server.id
                );
              this.dataSubject.value.data.servers[index] = response.data.server;
              this.filterSubject.next('');
              return { dataState: DataState.LOADED_STATE,appData: this.dataSubject.value}
            }),
            startWith({dataState: DataState.LOADED_STATE, appData: this.dataSubject.value}),
            catchError((error: string) => {
              this.filterSubject.next('');
              return of({dataState :DataState.ERROR_STATE,error})
            })
          );
  }

  filterServers(status: any): void{
    
    this.appState$ = this.serverService.filter$(status.target.value,this.dataSubject.value)
      .pipe(
        map(response => {
          return {dataState: DataState.LOADED_STATE,appData: response}
        }),
        startWith({dataState: DataState.LOADED_STATE,appData: this.dataSubject.value}),
        catchError((error: string) => {
          return of({dataState :DataState.ERROR_STATE,error})
        })
      );
  }


}
