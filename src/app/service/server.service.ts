import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import {tap,catchError} from 'rxjs/operators';
import { Status } from '../enum/status.enum';
import { CustomResponse } from '../interface/custom-response';
import { Server } from '../interface/server';

@Injectable({ providedIn: 'root'})
export class ServerService {
 
  private readonly apiUrl = 'http://localhost:8080';

  constructor(private http: HttpClient) { }

  /* 
  O metoda procedurala:
  getServers(): Observable<CustomRespnse> {
    return this.http.get<CustomRespnse>('http://localhost:8080/serverApp/list');


    using angular cli: ng generate service service/server
  }
*/

//o metoda mai "reactiva" folosind Reactive Extensions for JavaScript
//$-semnifica un Observable
//cast


  servers$ = <Observable<CustomResponse>>
              this.http.get<CustomResponse>(`${this.apiUrl}/serverApp/list`)
                        .pipe(
                          tap(console.log),
                          catchError(this.handleError)
                        );
//we use backquotes for pathVariable
/**
 * saves a server
 */
  save$ = (server: Server) => <Observable<CustomResponse>>
    this.http.post<CustomResponse>(`${this.apiUrl}/serverApp/save`,server)
                          .pipe(
                            tap(console.log),
                            catchError(this.handleError)
                          )


  ping$ = (ipAddress: string) => <Observable<CustomResponse>>
                            this.http.get<CustomResponse>(`${this.apiUrl}/serverApp/ping/${ipAddress}`)
                            .pipe(
                              tap(console.log),
                              catchError(this.handleError)
                            );
  
  /**
   * Filter servers by status.
  */
  filter$ = (status: Status,response: CustomResponse) => <Observable<CustomResponse>>
                              new Observable<CustomResponse>(
                                subscriber => {
                                  console.log(response);
                                  subscriber.next(
                                    status === Status.ALL ? { ...response,message: `Servers filtered by ${status} status`}
                                                          : 
                                                          {
                                                            ...response,
                                                            message: response.data.servers
                                                            .filter(server => server.status === status).length  > 0 ? 
                                                            `Servers filtered by ${status === Status.SERVER_UP ? 'SERVER UP' : 'SERVER DOWN' } status`
                                                            : `No server of ${status} found`,
                                                            data: {servers: response.data.servers
                                                                            .filter(server => server.status === status) }

                                                          }
                                  );
                                  subscriber.complete();
                                }
                              ).pipe(
                                tap(console.log),
                                catchError(this.handleError)
                              )
                        


  delete$ = (serverId: number)  => <Observable<CustomResponse>>
                              this.http.delete<CustomResponse>
                              (`${this.apiUrl}/serverApp/delete/${serverId}`)
                              .pipe(
                                tap(console.log),
                                catchError(this.handleError)
                              )
  
  private handleError(error: HttpErrorResponse): Observable<never> {
             console.log(error);
             return throwError(() => new Error(`Error occured - Error code: ${error.status}, Error message:${error.error.message}`));
           }

}
